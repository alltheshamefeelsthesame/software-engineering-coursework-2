package com;

import com.firstService.Schedule;

/**
 * Port modeling, SE #2 coursework
 * @version 0.1
 */
public class Main {

    public static void main(String[] args) {
        Schedule schedule = new Schedule();
        schedule.recordToFile();
    }

}
