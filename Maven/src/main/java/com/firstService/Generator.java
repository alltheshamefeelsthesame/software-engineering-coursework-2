package com.firstService;

import com.firstService.cargo.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Generator {

    public static final int CONTAINER_SPEED = randomInt(1, 10);
    public static final int LOOSE_SPEED = randomInt(1, 10);
    public static final int LIQUID_SPEED = randomInt(1, 10);

    private Generator() {
    }

    public static Ship[] generate() {

        int quantity = randomInt(1, 100);
        Ship[] ships = new Ship[quantity];
        for (int i = 0; i < quantity; ++i) {
            Cargo current = getCargo();
            ships[i] = new Ship(getDate(), getNameOfVessel(), current, unloadingTime(current));
        }

        return ships;
    }

    public static int randomInt(int min, int max) {
        return (int) (Math.random() * (++max - min) + min);
    }

    public static double randomDouble() {
        return Math.random() * 100 + 0.1;
    }

    private static String getNameOfVessel() {
        return "Ship #" + randomInt(1, 1000);
    }

    private static Calendar getDate() {
        return new GregorianCalendar(2021, Calendar.AUGUST,
                randomInt(1, 30), randomInt(0, 23), randomInt(0, 59));
    }

    private static Cargo getCargo() {

        return switch (randomInt(0, 2)) {
            case 0 -> new Cargo(CargoType.CONTAINER, randomInt(1, 1000));
            case 1 -> new Cargo(CargoType.LOOSE, randomDouble());
            case 2 -> new Cargo(CargoType.LIQUID, randomDouble());
            default -> null;
        };

    }

    private static int unloadingTime(Cargo cargo) {
        return switch (cargo.type()) {
            case CONTAINER -> (int) Math.ceil(cargo.quantity() / CONTAINER_SPEED);
            case LOOSE -> (int) Math.ceil(cargo.quantity() / LOOSE_SPEED);
            case LIQUID -> (int) Math.ceil(cargo.quantity() / LIQUID_SPEED);
        };
    }

}
