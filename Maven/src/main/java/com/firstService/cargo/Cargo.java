package com.firstService.cargo;

public record Cargo(CargoType type, double quantity) {
}
