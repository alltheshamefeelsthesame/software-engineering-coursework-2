package com.firstService.cargo;

public enum CargoType {

    CONTAINER, LOOSE, LIQUID

}
