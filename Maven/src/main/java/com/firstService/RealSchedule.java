package com.firstService;

import java.util.Calendar;

public class RealSchedule {

    Ship[] ships;

    public RealSchedule(Schedule schedule) {
        ships = schedule.getSchedule();

        for (var i : ships) {
            i.date.roll(Calendar.DAY_OF_MONTH, Generator.randomInt(-7,7));
            i.unloadingTime += Generator.randomInt(0, 1440);
        }

    }

}
