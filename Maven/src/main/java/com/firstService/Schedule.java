package com.firstService;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class Schedule {

    private Ship[] ships;

    public Schedule() {
        ships = Generator.generate();
        Arrays.sort(ships);
    }

    public Ship[] getSchedule() {
        return ships;
    }

    public void recordToFile() {

        Path file = Paths.get("test.md");

        try {
            if (Files.exists(file)) {
                Files.delete(file);
            }
            Files.createFile(file);

            for (var i : ships) {
                Files.writeString(file, i.toString() + "\n", StandardOpenOption.APPEND);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
