package com.firstService;

import com.firstService.cargo.Cargo;
import com.firstService.cargo.CargoType;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Ship implements Comparable<Ship> {

    public Calendar date;
    public int unloadingTime;
    private final String nameOfVessel;
    private final Cargo cargo;

    public Ship(Calendar date, String nameOfVessel, Cargo cargo, int unloadingTime) {
        this.date = date;
        this.nameOfVessel = nameOfVessel;
        this.cargo = cargo;
        this.unloadingTime = unloadingTime;
    }

    @Override
    public int compareTo(Ship current) {

        int[] first = new int[]{this.date.get(Calendar.MONTH), this.date.get(Calendar.DAY_OF_MONTH),
                this.date.get(Calendar.HOUR), this.date.get(Calendar.MINUTE)};
        int[] second = new int[]{current.date.get(Calendar.MONTH), current.date.get(Calendar.DAY_OF_MONTH),
                current.date.get(Calendar.HOUR), current.date.get(Calendar.MINUTE)};

        for (int i = 0; i < 4; ++i) {
            if (first[i] != second[i]) {
                return first[i] - second[i];
            }
        }

        return 0;
    }

    @Override
    public String toString() {

        SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        StringBuilder str = new StringBuilder(nameOfVessel + ", " +
                formatDate.format(date.getTime())).append(", cargo type ").append(cargo.type());

        if (cargo.type() == CargoType.CONTAINER) {
            str.append(", quantity of containers ").append((int) cargo.quantity());
        } else {
            str.append(", weight of cargo ").append(String.format("%.3f", cargo.quantity()));
        }

        return str.toString();
    }
}
